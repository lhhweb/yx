import qs from 'qs'
import crypto from 'crypto'
let getHash = function (para) {
  var keys = Object.keys(para)
  var newArray = keys.sort()
  var str = ''
  for (var i = 0; i < newArray.length; i++) {
    str = str + newArray[i] + para[newArray[i]]
  }
  var nStr = str + '2f3d74ebbac86bce982c3e771b2644ca'
  return md5(nStr)

  function md5 (str) {
    var md5 = crypto.createHash('md5')
    md5.update(str)
    return md5.digest('hex')
  }
}
export const domainConfig = {
  'localhost:8080': {
    url: 'http://xmap18120044.php.hzxmnet.com'
  }
}[window.location.host.toLowerCase()] || {
  // 测试部署地址
  url: 'http://xmap18120044.php.hzxmnet.com'
}

export const configAxios = {
  init (_axios) {
    _axios.defaults.baseURL = `${domainConfig.url}`
    _axios.interceptors.request.use(function (config) {
      global.vbus.$emit('loading_show', true)
      let MHlogin = sessionStorage.MHlogin ? JSON.parse(sessionStorage.MHlogin) : ''
      if (MHlogin && MHlogin.userToken) {
        config.headers['token'] = MHlogin.userToken
      }
      config.data = {
        ...config.data,
        apiId: '207cf7035d873cc91be18709397ec292',
        time: Date.parse(new Date()) / 1000,
        terminal: 4
      }
      let user = localStorage.HEROUSERINFO ? JSON.parse(localStorage.HEROUSERINFO) : ''
      if (user) {
        config.data.uid = user.uid
        config.data.hashid = user.hashid
      }
      config.data.hash = getHash(config.data)
      config.data = qs.stringify(config.data)
      return config
    }, function (error) {
      return Promise.reject(error)
    })
    _axios.interceptors.response.use(function (res) {
      global.vbus.$emit('loading_show', false)
      let data = res.data
      if (data.Code === '000000') {
        return data.Data
      } else {
        global.vbus.$emit('toast_show', data.Msg)
      }
    }, function (error) {
      global.vbus.$emit('loading_show', false)
      global.vbus.$emit('toast_show', '服务出错啦！')
      return Promise.reject(error)
    })
  }
}
