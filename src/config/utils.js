export default {
  http: 'http://xmap18120044.php.hzxmnet.com/Api/Ddk/show_captcha',
  timeFormate1: (time) => {
    let hour = Math.floor(time / 3600)
    let leftSeconds = time % 3600
    let minute = Math.floor(leftSeconds / 60)
    let seconds = leftSeconds % 60
    return (hour < 10 ? '0' + hour : hour) + ':' + (minute < 10 ? '0' + minute : minute) + ':' + (seconds < 10 ? '0' + seconds : seconds)
  }
}
