export default {
  message: {
    title: '啊实打实',
    back: '返回'
  },
  login: {
    title: '登录',
    puser: '请输入用户名',
    ppsd: '请输入密码',
    pcode: '请输入验证码',
    forget: '忘记密码',
    login: '登录',
    register: '注册新用户'
  },
  cmunity: {
    title: '热血英雄',
    tab1: '区块',
    tab2: '大奖池',
    tab3: '定时奖'
  },
  wallet: {
    title: '我的钱包',
    btn: '添加im Token钱包地址'
  },
  walletDt: {
    title: '我的钱包',
    block: '区块',
    blockval: '区块整价值',
    l1: '充值ETH',
    l2: '提现到imtoken钱包',
    l3: '转账',
    l4: '购买ROFK',
    l5: '交易记录',
    l6: '删除这个钱包'
  }
}
