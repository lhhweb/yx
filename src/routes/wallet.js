export default {
  path: '/wallet',
  component: resolve => {
    require(['@/views/Login'], resolve)
  },
  children: [
    {
      path: '/',
      component: resolve => {
        require(['@/views/Wallet/wallet'], resolve)
      }
    },
    {
      path: '/detail/:id',
      name: 'walletDetail',
      component: resolve => {
        require(['@/views/Wallet/detail'], resolve)
      }
    },
    {
      path: 'history',
      name: 'walletHistory',
      component: resolve => {
        require(['@/views/Wallet/history'], resolve)
      }
    }
  ]
}
