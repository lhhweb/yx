export default {
  path: '/',
  component: resolve => {
    require(['@/views/Login'], resolve)
  },
  children: [
    {
      path: 'login',
      component: resolve => {
        require(['@/views/Login/login'], resolve)
      }
    },
    {
      path: '/register',
      component: resolve => {
        require(['@/views/Login/register'], resolve)
      }
    },
    {
      path: '/forget',
      component: resolve => {
        require(['@/views/Login/forget'], resolve)
      }
    }
  ]
}
