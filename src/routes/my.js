export default {
  path: '/my',
  component: resolve => {
    require(['@/views/My'], resolve)
  },
  children: [
    {
      path: '/',
      component: resolve => {
        require(['@/views/My/my'], resolve)
      },
      meta: {
        keepAlive: true
      }
    },
    {
      path: 'userinfo',
      component: resolve => {
        require(['@/views/My/userinfo'], resolve)
      }
    },
    {
      path: 'team',
      component: resolve => {
        require(['@/views/My/team'], resolve)
      }
    },
    {
      path: 'link',
      redirect: '/myc/link'
    },
    {
      path: 'advice',
      redirect: '/myc/advice'
    },
    {
      path: 'notice',
      redirect: '/myc/notice'
    },
    {
      path: 'email',
      redirect: '/myc/email'
    },
    {
      path: 'rules',
      redirect: '/rules'
    }
  ]
}
