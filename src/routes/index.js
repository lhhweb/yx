import Vue from 'vue'
import Router from 'vue-router'
import Community from './community'
import Login from './login'
import My from './my'
import Myc from './myc'
import Wallet from './wallet'
Vue.use(Router)
const deployName = process.env.VUE_APP_DEPLOY_ROUTER_PATH
export default new Router({
  // mode: 'history',
  base: deployName,
  routes: [
    Community,
    Login,
    My,
    Myc,
    Wallet
  ]
})
