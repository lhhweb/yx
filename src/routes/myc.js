export default {
  path: '/myc',
  component: resolve => {
    require(['@/views/My/indexc'], resolve)
  },
  children: [
    {
      path: 'link',
      component: resolve => {
        require(['@/views/My/link'], resolve)
      },
      meta: {
        keepAlive: true
      }
    },
    {
      path: 'advice',
      component: resolve => {
        require(['@/views/My/advice'], resolve)
      },
      meta: {
        keepAlive: true
      }
    },
    {
      path: 'adviceDetail/:id',
      name: 'adviceDetail',
      component: resolve => {
        require(['@/views/My/adviceDetail'], resolve)
      }
    },
    {
      path: 'addAdvice',
      component: resolve => {
        require(['@/views/My/addAdvice.vue'], resolve)
      }
    },
    {
      path: 'notice',
      component: resolve => {
        require(['@/views/My/notice'], resolve)
      }
    },
    {
      path: 'email',
      component: resolve => {
        require(['@/views/My/email'], resolve)
      }
    },
    {
      path: 'emailDetail/:id',
      name: 'emailDetail',
      component: resolve => {
        require(['@/views/My/emailDetail'], resolve)
      }
    },
    {
      path: 'order',
      component: resolve => {
        require(['@/views/My/order'], resolve)
      }
    },
    {
      path: 'part',
      component: resolve => {
        require(['@/views/My/part'], resolve)
      }
    },
    {
      path: 'award',
      component: resolve => {
        require(['@/views/My/award'], resolve)
      }
    },
    {
      path: 'redpocket',
      component: resolve => {
        require(['@/views/My/redpocket'], resolve)
      }
    },
    {
      path: 'mypart/:id',
      name: 'partdetail',
      component: resolve => {
        require(['@/views/My/mypart'], resolve)
      }
    },
    {
      path: 'resetPsd',
      component: resolve => {
        require(['@/views/My/resetPsd'], resolve)
      }
    },
    {
      path: 'resetEmail',
      component: resolve => {
        require(['@/views/My/resetEmail'], resolve)
      }
    }
  ]
}
