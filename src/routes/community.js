export default {
  path: '/',
  component: resolve => {
    require(['@/views/Community'], resolve)
  },
  children: [
    {
      path: '/',
      component: resolve => {
        require(['@/views/Community/community'], resolve)
      },
      meta: {
        // keepAlive: true
      }
    },
    {
      path: '/rules',
      component: resolve => {
        require(['@/views/Community/rules'], resolve)
      }
    }
  ]
}
