/**
 * 引入组件
 */
import Header from './Header'
import Footer from './Footer'
import Collapse from './Collapse'
import Myhead from './Myhead'
const hview = {
  vhead: Header,
  vfoot: Footer,
  vcollapse: Collapse,
  vmyhead: Myhead
}

const install = Vue => {
  Object.keys(hview).forEach(key => Vue.component(key, hview[key]))
}

if (typeof window !== 'undefined' && window.Vue) install(window.Vue)

export default Object.assign(hview, {
  install
})
