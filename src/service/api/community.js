export default [
  {
    name: 'getCommunity',
    method: 'POST',
    desc: '社区首页信息',
    path: 'Api/Community/index',
    mockPath: '',
    params: {
      uid: '',
      hashid: ''
    }
  }
]
