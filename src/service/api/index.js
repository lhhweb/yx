import user from './user'
import community from './community'

export default {
  user,
  community
}
