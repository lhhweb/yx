import Axios from 'axios'
import { domainConfig } from '@/config/config-axios.js'

export default {
  // 获取钱包列表-所有的吧？
  getWallets (params) {
    let url = `${domainConfig.url}/Api/Wallet/wallet_list`
    return Axios.post(url, params)
  },
  // 添加钱包
  add (params) {
    let url = `${domainConfig.url}/Api/Wallet/add_wallet`
    return Axios.post(url, params)
  },
  // 删除钱包
  delete (params) {
    let url = `${domainConfig.url}/Api/Wallet/del_wallet`
    return Axios.post(url, params)
  },
  // 详情
  detail (params) {
    let url = `${domainConfig.url}/Api/Wallet/wallet_detail`
    return Axios.post(url, params)
  },
  // 充值eth
  rechargeEth (params) {
    let url = `${domainConfig.url}/Api/Wallet/recharge_eth`
    return Axios.post(url, params)
  },
  // 返回二维码
  getimtokenInfo (params) {
    let url = `${domainConfig.url}/Api/Wallet/sys_imtoken_info`
    return Axios.post(url, params)
  },
  // 提现
  getCash (params) {
    let url = `${domainConfig.url}/Api/Wallet/create_withdrawal`
    return Axios.post(url, params)
  },
  buyROFK (params) {
    let url = `${domainConfig.url}/Api/Wallet/buy_rofk`
    return Axios.post(url, params)
  },
  // 转账
  transfer (params) {
    let url = `${domainConfig.url}/Api/Wallet/transfer_account`
    return Axios.post(url, params)
  },
  // 交易记录
  chHistory (params) {
    let url = `${domainConfig.url}/Api/Wallet/trade_list`
    return Axios.post(url, params)
  }
}
