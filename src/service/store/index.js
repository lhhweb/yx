export default {
  state: {
    userIdentity: {
      // 用户信息
      token: null,
      userId: null
    },
    excludeArr: []
  },
  getters: {
    userToken: state => {
      return state.userIdentity.token
    },
    userId: state => {
      return state.userIdentity.userId
    }
  },
  mutations: {
    muUserMetaInfo (state, userInfo) {
      state.userIdentity = { ...state.userIdentity, ...userInfo }
    },
    muRemoveExclude (store, data) {
      let index = store.excludeArr.indexOf(data)
      if (index !== -1) {
        store.excludeArr.splice(index, 1)
      }
    },
    muAddExclude (store, data) {
      store.excludeArr = [...store.excludeArr, data]
    }
  },
  actions: {}
}
