import Axios from 'axios'
import { domainConfig } from '@/config/config-axios.js'

export default {
  // 个人中心
  center (params) {
    let url = `${domainConfig.url}/Api/Sysuser/ucenter`
    return Axios.post(url, params)
  },
  // 注册
  register (params) {
    let url = `${domainConfig.url}/Api/Sysuser/register`
    return Axios.post(url, params)
  },
  // 登录
  login (params) {
    let url = `${domainConfig.url}/Api/Sysuser/login`
    return Axios.post(url, params)
  },
  // 找回密码
  getBack (params) {
    let url = `${domainConfig.url}/Api/Sysuser/retrieve_password`
    return Axios.post(url, params)
  },
  // 我的区块头部
  blockHead (params) {
    let url = `${domainConfig.url}/Api/Community/my_block_top`
    return Axios.post(url, params)
  },
  // 我的区块列表
  blocks (params) {
    let url = `${domainConfig.url}/Api/Community/my_block_list`
    return Axios.post(url, params)
  },
  // rofk待处理
  rofkUncancel (params) {
    let url = `${domainConfig.url}/Api/Sysuser/rofk_order_pending_list`
    return Axios.post(url, params)
  },
  // 拒绝卖
  refuseSell (params) {
    let url = `${domainConfig.url}/Api/Sysuser/rofk_sell_refuse`
    return Axios.post(url, params)
  },
  // 同意卖
  agreeSell (params) {
    let url = `${domainConfig.url}/Api/Sysuser/rofk_sell_do`
    return Axios.post(url, params)
  },
  // rofk已完成
  rofkOver (params) {
    let url = `${domainConfig.url}/Api/Sysuser/rofk_order_completed_list`
    return Axios.post(url, params)
  },
  // 记录
  rofkRecord (params) {
    let url = `${domainConfig.url}/Api/Sysuser/rofk_order_record`
    return Axios.post(url, params)
  },
  // 我的红包
  redPacket (params) {
    let url = `${domainConfig.url}/Api/Community/my_redpacket`
    return Axios.post(url, params)
  },
  // 红包复投
  redPacketFutou (params) {
    let url = `${domainConfig.url}/Api/Community/redpacket_redelivery`
    return Axios.post(url, params)
  },
  // 红包领取
  redPacketGetcash (params) {
    let url = `${domainConfig.url}/Api/Community/redpacket_take`
    return Axios.post(url, params)
  },
  // 王者费用
  getkingCost (params) {
    let url = `${domainConfig.url}/Api/Sysuser/get_buykingsecretkey_cost`
    return Axios.post(url, params)
  },
  // 购买王者
  buyking (params) {
    let url = `${domainConfig.url}/Api/Sysuser/buy_king_secretkey`
    return Axios.post(url, params)
  },
  // 我的团队
  team (params) {
    let url = `${domainConfig.url}/Api/Sysuser/my_team`
    return Axios.post(url, params)
  },
  // 我的团队-收益列表
  teamList1 (params) {
    let url = `${domainConfig.url}/Api/Sysuser/team_list1`
    return Axios.post(url, params)
  },
  teamList1Detail (params) {
    let url = `${domainConfig.url}/Api/Sysuser/team_list2`
    return Axios.post(url, params)
  },
  // 反馈信息
  addadvice (params) {
    let url = `${domainConfig.url}/Api/Feedback/writeDo`
    return Axios.post(url, params)
  },
  getadvice (params) {
    let url = `${domainConfig.url}/Api/Feedback/get_list`
    return Axios.post(url, params)
  },
  detailadvice (params) {
    let url = `${domainConfig.url}/Api/Feedback/get_detail`
    return Axios.post(url, params)
  },
  // 图片上传
  uploadImgs (params) {
    let url = `${domainConfig.url}/api/upload/uploadImg`
    return Axios.post(url, params)
  },
  // 系统公告
  sysAdvertise (params) {
    let url = `${domainConfig.url}/Api/Sysuser/announcement`
    return Axios.post(url, params)
  },
  // 邮箱信息列表
  sysEmails (params) {
    let url = `${domainConfig.url}/Api/Sysuser/sysemail_list`
    return Axios.post(url, params)
  },
  // 邮箱信息详情
  sysEmailsDetail (params) {
    let url = `${domainConfig.url}/Api/Sysuser/sysemail_detail`
    return Axios.post(url, params)
  }
}
