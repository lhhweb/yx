import Axios from 'axios'
import { domainConfig } from '@/config/config-axios.js'
// import qs from 'qs'

export default {
  // 社区首页信息获取
  getInfo (params) {
    let url = `${domainConfig.url}/Api/Community/index`
    return Axios.post(url, params)
  },
  // 大奖池
  bigPrizePool (params) {
    let url = `${domainConfig.url}/Api/Community/big_prize_pool`
    return Axios.post(url, params)
  },
  timePrice (params) {
    let url = `${domainConfig.url}/Api/Community/timing_prize`
    return Axios.post(url, params)
  },
  // 购买区块
  buyBlock (params) {
    let url = `${domainConfig.url}/Api/Community/buy_block`
    return Axios.post(url, params)
  },
  // banner
  banner (params) {
    let url = `${domainConfig.url}/Api/Community/get_banner`
    return Axios.post(url, params)
  }
}
