import Vue from 'vue'
import App from './App'
import router from './routes'
import './global'
import store from '@/plugins/store'
import { apSetTitle } from '@/module/appbridge'
import hView from '@/components'
// axios配置
import Axios from 'axios'
import { configAxios } from '@/config/config-axios'
import VueI18n from 'vue-i18n'
import zh from '@/config/i18n/lang/zh'
import en from '@/config/i18n/lang/en'
Vue.use(VueI18n)

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
    apSetTitle(to.meta.title)
  }
  next()
})

// 自定义组件
Vue.use(hView)
// axios-自定义配置
configAxios.init(Axios)
Object.defineProperty(Vue.prototype, '$http', {
  value: Axios
})
const messages = {
  'zh': zh,
  'en': en
}
const i18n = new VueI18n({
  locale: 'zh',
  messages
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  store,
  render: h => h(App)
})
