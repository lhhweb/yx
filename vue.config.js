const env = process.env
const isProd = process.env.NODE_ENV === 'production'

module.exports = {
  productionSourceMap: false,
  baseUrl: process.env.NODE_ENV === 'production' ? `${env.WEBPACK_PATH}` : './',
  configureWebpack: {
    externals: isProd ? {
      'vue': 'Vue',
      'vue-router': 'VueRouter',
      'vuex': 'Vuex',
      'axios': 'axios'
    } : {}
  },
  transpileDependencies: [
    /\/node_modules\/vue-echarts\//,
    /\/node_modules\/resize-detector\//
  ]
}
